# E-Commerce

# for login

userName -user@gmail.com
password -1234567

## Description

I've used Firebase for authentication, Redux for state management, and React Navigation for navigation. Firebase is a popular platform for building web and mobile apps, providing tools for authentication, real-time databases, cloud storage, and more.

Redux is a state management library that is commonly used in React applications. It allows you to manage application state in a centralized location, making it easier to maintain and debug your code.

React Navigation is a library for navigating between screens in a React Native application. It provides a simple and customizable API for handling navigation and offers features such as stack navigation, tab navigation, and drawer navigation.

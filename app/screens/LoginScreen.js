import AuthContent from '../component/screen/Auth/AuthContent';
import {useContext, useEffect, useState} from 'react';
import LoadingOverlay from '../component/ui/LoadindOverlay';
import {Login} from '../util/auth';
import {Alert} from 'react-native';
import {useDispatch} from 'react-redux';
import {authActions} from '../../store/authSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';

function LoginScreen() {
  const [loginToken, setLoginToken] = useState('');

  useEffect(() => {
    async function fetchToken() {
      const storedToken = await AsyncStorage.getItem('token');
      if (storedToken) {
        dispatch(
          authActions.login({
            token: storedToken,
          }),
        );
      }
    }
    fetchToken();
  }, []);
  const dispatch = useDispatch();
  const [isAuthenticate, setIsAuthenticate] = useState(false);

  async function loginHandler({email, password}) {
    setIsAuthenticate(true);
    try {
      const token = await Login({email, password});
      setLoginToken(token);
      dispatch(
        authActions.login({
          token: token,
        }),
      );
      AsyncStorage.setItem('token', token);
    } catch (error) {
      Alert.alert('Authentication Failed!', 'Could not log you in.');
    }
    setIsAuthenticate(false);
  }
  if (isAuthenticate) {
    return <LoadingOverlay message="Creating user..." />;
  }
  return <AuthContent isLogin onAuthenticate={loginHandler} />;
}

export default LoginScreen;

import {FlatList, StyleSheet} from 'react-native';
import ProductList from '../component/screen/Auth/mainScreen/ProductList';

const DUMMY_PRODUCTS = [
  {
    id: 1,
    name: 'MacBook',
    imgURL:
      'https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bGFwdG9wfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    price: 25,
    offerPrice:23
  },
  {
    id: 2,
    name: 'Lenovo Yoga',
    imgURL:
      'https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bGFwdG9wfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    price: 25,
    offerPrice:23
  },
  {
    id: 3,
    name: 'Dell lattitude',
    imgURL:
      'https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bGFwdG9wfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    price: 25,
    offerPrice:23
  },
  {
    id: 4,
    name: 'HP Pavillion',
    imgURL:
      'https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bGFwdG9wfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    price: 25,
    offerPrice:23
  },
  {
    id: 5,
    name: 'Acer Aspire',
    imgURL:
      'https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bGFwdG9wfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    price: 25,
    offerPrice:23
  },
  {
    id: 6,
    name: 'Acer',
    imgURL:
      'https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bGFwdG9wfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    price: 35,
    offerPrice:33
  },
];

function WelcomeScreen({navigation}) {
  return (
    <FlatList
      data={DUMMY_PRODUCTS}
      renderItem={({item}) => <ProductList product={item} />}
      numColumns={2}
      keyExtractor={item => item.id}
    />
  );
}

export default WelcomeScreen;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 32,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 8,
  },
});

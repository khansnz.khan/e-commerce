import { useContext, useState } from 'react';
import AuthContent from '../component/screen/Auth/AuthContent'
import { CreateUser } from '../util/auth';
import LoadingOverlay from '../component/ui/LoadindOverlay'
import { Alert } from 'react-native';
import { useDispatch } from 'react-redux';
import { authActions } from '../../store/authSlice';

function SignupScreen() {
  const[isAuthenticate,setIsAuthenticate] = useState(false)

    const dispatch = useDispatch();

  async function signupHandler({email,password}){
    setIsAuthenticate(true)
    try {
      
      const token = await CreateUser({email,password});
      dispatch(authActions.login({
        token:token
      }))
    } catch (error) {
      Alert.alert('Authentication Failed',
      'Could not create user, please check your input and try again later.')
    }
   setIsAuthenticate(false)
  }
  if(isAuthenticate){
    return <LoadingOverlay message='Creating user...' />
  }
  return <AuthContent onAuthenticate={signupHandler} />;
}

export default SignupScreen;
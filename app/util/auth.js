import axios from 'axios';

const API_KEY = 'AIzaSyCIYw3B48TdDN3wtAk7NLaV7P9jFNmB7pw';

export async function CreateUser(email, password) {
  const response = await axios.post(
    'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + API_KEY,
    email,
    password,
    {returnSecureToken: true},
  );
  const token = response.data.idToken;
  return token;
}

export async function Login(email, password) {
  const response = await axios.post(
    'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' +
      API_KEY,
    email,
    password,
    {returnSecureToken: true},
  );
  const token = response.data.idToken;
  return token;
}

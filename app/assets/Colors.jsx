// Light|Lighter|Lightest,Dark|Darker|Darkest

export default Colors = {
  blackDarkest: '#20232B',
  blackDarker:'#181A20',
  blackDark: '#20232BCC',
  black:'#1D2E19',

  whiteDarkest: '#FFFFFF',
  whiteLight: '#9CAAB7',

  yellowDarkest: '#FFCF0A',
  yellowDark: '#FFCC00',
  yellow:'#D7AA36',

  blueDarkest: '#232C45',
  blueDarker: '#32436C',

  blueDark: '#182739',
  blueLight: '#1B5B9B',

  blueLighter: '#6798C9',

  green:'#9BD025',
  greenDarkest: '#749824',

  greyLight:'#9CAAB7',
  greyDark:'#627E9B',
  greyDarker:'#263351',
  greyDarkest:'#1F222A',
  grey:'#D2DBE42E',
  midGrey:'#324961',

  greyish:'#232C45'
  
};

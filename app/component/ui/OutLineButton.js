import { View, Text, Pressable, StyleSheet } from 'react-native'
import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Colors from '../../assets/Colors'

const OutLineButton = ({children,icon,onPress}) => {
  return (
   <Pressable onPress={onPress} style={({pressed})=>[ styles.button,pressed && styles.pressed ]}>
    <Ionicons
     style={styles.icon}
     name={icon}
     size={18}
     color={Colors.blackDarkest}
     />
    <Text style={styles.text} >{children}</Text>
   </Pressable>
  )
}

export default OutLineButton
const styles = StyleSheet.create({
    button:{paddingHorizontal:12,
    paddingVertical:6,
margin:4,
flexDirection:'row',
justifyContent:"center",
alignItems:'center',
borderWidth:1,
borderColor:Colors.greyDarkest
},
    pressed:{
        opacity:0.7
    },
    icon:{
        marginRight:6
    },
    text:{
        color:Colors.blackDarkest
    }
})
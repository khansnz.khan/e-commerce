import {
  View,
  Text,
  Pressable,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

export default function ProductList({product}) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate('ProductScreen', {
          id: product?.id,
          name: product?.name,
          image: product?.imgURL,
          price: product?.price,
          offerPrice: product?.offerPrice,
        });
      }}>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{uri: product.imgURL}} />
      </View>
      <View style={styles.innerContainer}>
        <Text style={styles.text}>{product.name}</Text>
        <Text style={styles.text}>${product.price}</Text>
      </View>
      <Text style={styles.offerText}>offer price ${product.offerPrice}</Text>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    margin: 2,
    width: '45%',
    height: 320,
  },
  imageContainer: {
    width: '100%',
    height: 250,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
  },
  text: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
  },
  offerText: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom:3
  },
});

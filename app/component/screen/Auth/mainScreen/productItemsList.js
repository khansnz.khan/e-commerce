import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';
import Button from '../../../ui/Button';
import {useDispatch, useSelector} from 'react-redux';
import {productListActions} from '../../../../../store/productList';
import {useNavigation} from '@react-navigation/native';
export default function ProductItemsList({id, name, price, quantity}) {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn);
  const navigation = useNavigation();

  const increments = () => {
    isLoggedIn.token
      ? dispatch(
          productListActions.addToCart({
            id,
            name,
            price,
          }),
        )
      : navigation.navigate('Login');
  };
  const decrements = () => {
    isLoggedIn.token
      ? dispatch(productListActions.removeFromCart(id))
      : navigation.navigate('Login');
  };
  const deleteItem = () => {
    isLoggedIn.token
      ? dispatch(productListActions.deleteFromCart(id))
      : navigation.navigate('Login');
  };

  return (
    <View style={styles.conatiner}>
      {/* <Image style={styles.image} source={{uri: image}} /> */}
      <View style={{height: '100%', justifyContent: 'space-evenly'}}>
        <Text style={[styles.text, {fontSize: 24}]}>{name}</Text>
        <View style={styles.innerContainer}>
          <Button onPress={decrements}>-</Button>
          <Text style={styles.text}>{quantity}</Text>
          <Button onPress={increments}>+</Button>
        </View>
        <Button style={styles.deleteButton} onPress={deleteItem}>
          Delete
        </Button>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  conatiner: {
    height: 200,
    width: '90%',
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: 'row',
  },
  image: {
    height: '100%',
    width: '40%',
    marginRight: 10,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  text: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
  },
  deleteButton: {
    marginTop: 10,
  },
});

import {View, Text, Image, StyleSheet} from 'react-native';
import React from 'react';
import Button from '../../../ui/Button';
import {useDispatch, useSelector} from 'react-redux';
import {productListActions} from '../../../../../store/productList';
export default function ProductScreen({route, navigation}) {
  const {id, name, image, price, offerPrice} = route.params;
  const cartItems = useSelector(state => state.list.itemsList);
  const quantity = useSelector(state => state.list.totalQty);
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn);
  const dispatch = useDispatch();

  const addToCarts = () => {
    dispatch(
      productListActions.addToCart({
        name,
        id,
        price,
      }),
    );
  };
  const buyLaptop = () => {
    dispatch(
      productListActions.addToCart({
        name,
        id,
        price,
      }),
    );
    navigation.navigate('ProductBuy', {
      id: id,
      name: name,
      image: image,
      price: price,
    });
  };
  return (
    <View style={styles.container}>
      <Button
        style={styles.button}
        onPress={() => {
          navigation.navigate('ProductCart');
        }}>
        Cart : {quantity} Items
      </Button>
      <Image style={styles.image} source={{uri: image}} />
      <View style={styles.innerContainer}>
        <Text style={styles.text}>{name}</Text>
        <Text style={styles.text}>${price}</Text>
      </View>
      <Text style={styles.offerText}>offer price ${offerPrice}</Text>
      <View style={styles.innerContainer}>
        <Button onPress={addToCarts}>Add to cart</Button>
        <Button
          onPress={() => {
            isLoggedIn.token ? buyLaptop() : navigation.navigate('Login');
          }}>
          {' '}
          Buy now
        </Button>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 10,
  },
  image: {
    width: '99%',
    height: 500,
    padding: 10,
    marginTop: 5,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
    paddingHorizontal: 30,
  },
  text: {
    color: 'black',
    fontSize: 24,
    fontWeight: 'bold',
  },
  button: {
    marginTop: 20,
    paddingHorizontal: 30,
  },
  offerText: {
    color: 'black',
    fontSize: 24,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 3,
  },
});

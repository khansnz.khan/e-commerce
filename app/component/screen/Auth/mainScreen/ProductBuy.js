import {View, Text, StyleSheet, Image, FlatList} from 'react-native';
import React from 'react';
import {useDispatch,useSelector} from 'react-redux';
import { productActions } from '../../../../../store/productDetails';
import ProductItemsList from './productItemsList';
export default function ProductCart({route}) {
  const {id, name, image, price} = route.params;
  const dispatch = useDispatch();
  const cartItems = useSelector(state => state.list.itemsList);

  const increments = () => {
    dispatch(productActions.increment())
  };
  const decrements = () => {
    dispatch(productActions.decrement())

  };
  return (
    <FlatList
    data={cartItems}
    renderItem={({item}) => <ProductItemsList id ={item.id} price = {item.price} total={item.totalPrice} name = { item.name} quantity={item.quantity}
    />} 
    keyExtractor={(item) =>  item.id}

    />
  );
}
const styles = StyleSheet.create({
  conatiner: {
    height: 200,
    width: '90%',
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: 'row',
  },
  image: {
    height: '100%',
    width: '40%',
    marginRight: 10,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  text: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
  },
  deleteButton: {
    marginTop: 10,
  },
});

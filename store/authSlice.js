import { createSlice} from '@reduxjs/toolkit'
import AsyncStorage from '@react-native-async-storage/async-storage'

const authSlice = createSlice ({
    name : 'auth',
    initialState:{isLoggedIn: {}},
    reducers:{
        login(state,action){
            state.isLoggedIn = {
                token :action.payload.token,
            }
        },
        logout(state){
            state.isLoggedIn = {}
        }
    }
})
export const authActions = authSlice.actions;
export default authSlice;

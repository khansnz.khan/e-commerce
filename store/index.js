import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./authSlice";
import productDetails from "./productDetails";
import productList from "./productList";

const store = configureStore({
    reducer:{
        auth:authSlice.reducer,
        product: productDetails.reducer,
        list : productList.reducer
    }
})
export default store;
import {createSlice} from '@reduxjs/toolkit';

const productDetails = createSlice({
  name: 'productDetails',
  initialState: {counter: 1},
  reducers: {
    increment(state, action) {
        state.counter ++
    },
    decrement(state, action) {
      state.counter --;
    },
  },
});
export const productActions = productDetails.actions;
export default productDetails;

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from './app/screens/LoginScreen';
import SignupScreen from './app/screens/SignupScreen';
import WelcomeScreen from './app/screens/WelcomeScreen';
import {Colors} from './constants/styles';
import {Provider, useSelector, useDispatch} from 'react-redux';
import store from './store';
import ProductScreen from './app/component/screen/Auth/mainScreen/ProductScreen';
import ProductBuy from './app/component/screen/Auth/mainScreen/ProductBuy';
import ProductCart from './app/component/screen/Auth/mainScreen/ProductCart';
import ProductItemsList from './app/component/screen/Auth/mainScreen/productItemsList';
import IconButton from './app/component/ui/IconButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {authActions} from './store/authSlice';
import {Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
const Stack = createNativeStackNavigator();

function AuthStack() {
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn);
  const navigation = useNavigation();
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: Colors.primary500},
        headerTintColor: 'white',
        contentStyle: {backgroundColor: Colors.primary100},
      }}>
      <Stack.Screen
        name="Welcome"
        component={WelcomeScreen}
        options={{
          headerTitle: () => (
            <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
              {'Please Login'}
            </Text>
          ),
          headerRight: () => (
            <IconButton
              onPress={() => navigation.navigate('Login')}
              icon={'log-in'}
              color={'white'}
              size={30}
            />
          ),
        }}
      />
      <Stack.Screen name="ProductScreen" component={ProductScreen} />
      <Stack.Screen name="ProductCart" component={ProductCart} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Signup" component={SignupScreen} />
    </Stack.Navigator>
  );
}

function AuthenticatedStack() {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn);

  const logoutHandler = () => {
    AsyncStorage.removeItem('token');
    dispatch(authActions.logout());
  };
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: Colors.primary500},
        headerTintColor: 'white',
        contentStyle: {backgroundColor: Colors.primary100},
      }}>
      <Stack.Screen
        name="Welcome"
        component={WelcomeScreen}
        options={{
          headerTitle: () => (
            <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
              {isLoggedIn && 'Welcome to Laptops market '}
            </Text>
          ),
          headerRight: () => (
            <IconButton
              onPress={() => logoutHandler()}
              icon={'log-out'}
              color={'darkPink'}
              size={30}
            />
          ),
        }}
      />
      <Stack.Screen name="ProductScreen" component={ProductScreen} />
      <Stack.Screen name="ProductBuy" component={ProductBuy} />
      <Stack.Screen name="ProductCart" component={ProductCart} />
      <Stack.Screen name="ProductItemsList" component={ProductItemsList} />
    </Stack.Navigator>
  );
}

function Navigation() {
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn);
  return (
    <NavigationContainer>
      {!isLoggedIn.token && <AuthStack />}
      {isLoggedIn.token && <AuthenticatedStack />}
    </NavigationContainer>
  );
}

function App() {
  return (
    <>
      <Navigation />
    </>
  );
}
export default () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};
